<?php

namespace ACF_Helper_Generator;

if( ! class_exists( 'ACF_Helper_Generator\Util' ) ) {

    /**
     * Utility class providing helper methods for various tasks.
     *
     * @package Acf Helper generator
     * @subpackage ACF_Helper_Generator
     * @since 1.0.0
     */
    class Util {

        /**
         * Concatenates a list of strings with a specified delimiter.
         *
         * @param   string ...$strings The strings to be concatenated.
         * @param   string $delimiter  The delimiter to be used between the strings, defaults to "_".
         *
         * @return  string The concatenated string.
         */
        public static function concat_strings( string $delimiter = "_", ...$strings ) {
            foreach ( $strings as $string ) {
                if ( ! is_string($string) ) {
                    throw new \TypeError('All parameters should be of type string.');
                }
            }
            return implode($delimiter, $strings);
        }

        /**
         * Check if the date passed is a valid date
         *
         * @param   string  $date The date to check
         *
         * @return  bool    True if the date passed is a valid date, false otherwise
         */
        public static function is_date_valid( string $date ) : bool {
            return (bool)strtotime($date);
        }

        /**
         * Convert date in any other valid datetime format
         *
         * @param   string $datetime        The datetime to convert
         * @param   string $datetime_format The desired datetime format
         *
         * @return  string The converted datetime, or the orginal datetime if any error
         */
        public static function convert_datetime( string $datetime, string $datetime_format = 'd-m-Y H:i' ) : string {
            $timestamp  = strtotime( $datetime );
            $new_datetime = date( $datetime_format, $timestamp );

            if( ! self::is_date_valid( $new_datetime ) ) return $datetime;
            return $new_datetime;
        }

        /**
         * Convert Media to another Media format
         *
         * @param string|int|array $media_to_convert The Media to convert to another format
         * @param $return_format                     The desired Media format
         *
         * @return string|int|array|bool             The Media converted in the desired format, or False on failure.
         */
        public static function convert_media( string|int|array $media_to_convert, string $return_format = 'ids' ): string|int|array|bool {

            switch( $return_format ) {
                case 'url':
                    if( is_array( $media_to_convert ) ) {
                        $media_to_convert = $media_to_convert['url'];
                    } elseif( Util::is_int( $media_to_convert ) ) {
                        $media_to_convert = wp_get_attachment_url( $media_to_convert ); // Return false on error
                    }
                    break;
                case 'array':
                    if( ! is_array( $media_to_convert ) ) {
                        $media_id = intval( self::convert_media( $media_to_convert, 'ids' ) );
                        $media_to_convert = acf_get_attachment( $media_id ); // Return false on error
                    }
                    break;
                default: // ids
                    if( is_array( $media_to_convert ) ) {
                        $media_to_convert = $media_to_convert['ID'];
                    } elseif( wp_http_validate_url( $media_to_convert ) ) {
                        $media_to_convert = attachment_url_to_postid( $media_to_convert );
                        if( empty( $media_to_convert ) ) {
                            $$media_to_convert = false;
                        }
                    }
                    break;
            }
            return $media_to_convert;
        }

        /**
         * Convert User to another user format
         *
         * @param   int|array|WP_User $user_to_convert The user to convert to another format
         * @param   string            $return_format   The desired user format
         *
         * @return  int|array|\WP_User                 The user converted in the desired format
         */
        public static function convert_user( int|array|\WP_User $user_to_convert, string $return_format = 'object' ): int|array|\WP_User {
            $user = '';

            switch( $return_format ) {
                case 'ids':
                    if( is_array( $user_to_convert ) ) {
                        $user = $user_to_convert['ID'];
                    } elseif( is_object( $user_to_convert ) ) {
                        $user = $user_to_convert->ID;
                    } else {
                        $user = $user_to_convert;
                    }
                    break;
                case 'array':
                    if( is_array( $user_to_convert ) ) {
                        $user = $user_to_convert;
                    } elseif( is_object( $user_to_convert ) ) {
                        $user = $user_to_convert->to_array();
                    } else {
                        $user = get_user_by( 'id', $user_to_convert )->to_array();
                    }
                    break;
                default: // object
                    if( is_array( $user_to_convert ) ) {
                        $user = get_user_by( 'id', $user_to_convert['ID'] );
                    } elseif( is_object( $user_to_convert ) ) {
                        $user = $user_to_convert;
                    } else {
                        $user = get_user_by( 'id', $user_to_convert );
                    }
                    break;
            }
            return $user;
        }

        /**
         * Check if the variable passed is of type 'WP_Post'
         *
         * @param   mixed   $var The variable to check
         *
         * @return  bool    True if the variable is of type 'WP_Post', false otherwise
         */
        public static function is_post( $var ) : bool {
            return is_a( $var, 'WP_Post' );
        }

        /**
         * Check if the variable passed is of type 'int'
         *
         * @param   mixed   $var The variable to check
         *
         * @return  bool    True if the variable is of type 'int', false otherwise
         */
        public static function is_int( $var ) : bool {
            return is_int( $var ) || ctype_digit( $var );
        }

        /**
         * Format long text by converting newlines to <br> and escaping HTML characters
         * 
         * @param   string $text The text to be formatted
         * 
         * @return  string The formatted text
        */
        public static function format_long_text( string $text ) : string {
            return wpautop( $text );
        }
        
        /**
         * Check if a post exists
         * @param   int|string|\WP_Post $post The post
         * 
         * @return  bool                True if the post exists, false otherwise
        */
        public static function post_exists( int|string|\WP_Post $post ) : bool {
            return is_string( get_post_status( $post ) );
        }

        /**
         * Retrieves the ID of a post.
         *
         * @param  WP_Post|int  $post The post object or post ID.
         *
         * @return int|bool     The ID of the post or false if the post does not exist.
         */
        public static function get_post_id( int|string|\WP_Post $post ) : int|bool {
            if( self::is_post( $post ) ) {
                return $post->ID;
            }

            if( ( is_integer( $post ) || ctype_digit( $post ) ) && self::post_exists( $post ) ) {
                return $post;
            }

            return false;
        }
        
    }
}
?>
    