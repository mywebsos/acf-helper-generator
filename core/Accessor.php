<?php

namespace ACF_Helper_Generator;

defined( 'ABSPATH' ) || exit;

if( ! class_exists( 'ACF_Helper_Generator\Accessor' ) ) {

    /**
     * A utility class that allows access and modification of ACF fields
     *
     * This class represents a utility class that allows access and 
     * modification of ACF fields according to their type.
     *
     * @package Acf Helper generator
     * @subpackage ACF_Helper_Generator
     * @since 1.0.0
     */
    class Accessor {

        /**
         * Retrieves the value of a Email field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string               The field value or an empty string on error.
         */
        public static function get_email( string $field_name, int|string|\WP_Post $post ) : string {
            return sanitize_email( self::get_text( $field_name, $post ) );
        }

        /**
         * Set the value of a Email field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post   $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_email( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            $value = sanitize_email( $value );

            if( ! is_email( $value ) ) return false;
            return self::set_text( $field_name, $post, $value );
        }

        /**
         * Retrieves the value of a Number field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string               The field value or an empty string on error.
         */
        public static function get_number( string$field_name, int|string|\WP_Post $post ) : string {
            return self::get_text( $field_name, $post );
        }

        /**
         * Set the value of a Number field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post  $post          The post ID or WP_Post object
         * @param   string|int|float     $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_number( string $field_name, int|string|\WP_Post $post, string|int|float $value ) : bool {
            $field_object = get_field_object( $field_name, $post );

            if( is_array( $field_object ) ) {

                if( ! is_numeric( $value ) ) return false;

                if( isset( $field_object['step'] ) && is_number( $field_object['step'] ) ) {
                    $step = floatval( $field_object['step'] );
                    if( floatval( $value ) % $step !== 0 ) {
                        return false;
                    }
                }

                if( isset( $field_object['min'] ) && is_number( $field_object['min'] ) ) {
                    $min = floatval( $field_object['min'] );
                    if( floatval( $value ) < $min ) {
                        return false;
                    }
                }
                if( isset( $field_object['max'] ) && is_number( $field_object['max'] ) ) {
                    $max = floatval( $field_object['max'] );
                    if( floatval( $value ) > $max ) {
                        return false;
                    }
                }
            }

            return self::set_text( $field_name, $post, strval( $value ) );
        }

        /**
         * Retrieves the value of a Password field for a given post.
         * 
         * @param string                $field_name The name of the field
         * @param int|string|\WP_Post   $post       The post ID or WP_Post object
         * 
         * @return string               The field value or an empty string on error.
         */
        public static function get_password( string $field_name, int|string|\WP_Post $post ) : string {
            return self::get_text( $field_name, $post );
        }

        /**
         * Set the value of a Password field for a given post.
         * 
         * @param   string              $field_name     The name of the field
         * @param   int|string|\WP_Post $post           The post ID or WP_Post object
         * @param   string              $value          The new value
         * 
         * @return  bool                True on successful update, false on failure.
         */
        public static function set_password( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            return self::set_text( $field_name, $post, $value );
        }

        /**
         * Retrieves the value of a Range field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string               The field value or an empty string on error.
         */
        public static function get_range( string$field_name, int|string|\WP_Post $post ) : string {
            return self::get_number( $field_name, $post );
        }

        /**
         * Set the value of a Range field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post  $post          The post ID or WP_Post object
         * @param   string|int|float     $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_range( string $field_name, int|string|\WP_Post $post, string|int|float $value ) : bool {
            return self::set_number( $field_name, $post, strval( $value ) );
        }

        /**
         * Retrieves the value of a Text field for a given post.
         * 
         * @param string                $field_name The name of the field
         * @param int|string|\WP_Post   $post       The post ID or WP_Post object
         * 
         * @return string               The field value or an empty string on error.
         */
        public static function get_text( string $field_name, int|string|\WP_Post $post ) : string {
            $post_id = Util::get_post_id( $post );
            if( false !== $post_id ) {
                $value = get_field( $field_name, $post_id );
                return is_null( $value ) ? '' : sanitize_text_field( $value );
            }   

            return '';
        }

        /**
         * Set the value of a Text field for a given post.
         * 
         * @param   string              $field_name     The name of the field
         * @param   int|string|\WP_Post $post           The post ID or WP_Post object
         * @param   string              $value          The new value
         * 
         * @return  bool                True on successful update, false on failure.
         */
        public static function set_text( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                return update_field( $field_name, sanitize_text_field( $value ), $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a TextArea field for a given post.
         * 
         * @param string                $field_name The name of the field
         * @param int|string|\WP_Post   $post       The post ID or WP_Post object
         * 
         * @return string               The field value or an empty string on error.
         */
        public static function get_textarea( string $field_name, int|string|\WP_Post $post ) : string {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                $value = get_field( $field_name, $post_id );
                return is_null( $value ) ? '' : Util::format_long_text( $value );
            }   

            return '';
        }

        /**
         * Set the value of a TextArea field for a given post.
         * 
         * @param string             $field_name The name of the field
         * @param int|string|\WP_Post $post The post ID or WP_Post object
         * @param string             $value The field value
         * 
         * @return bool              True on successful update, false on failure.
         */
        public static function set_textarea( string $field_name, int|string|\WP_Post $post, string $value ): bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                return update_field( $field_name, $value, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a Url field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_url( string $field_name, int|string|\WP_Post $post ) : string {
            return sanitize_url( self::get_text( $field_name, $post ) );
        }

        /**
         * Set the value of a Url field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post  $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_url( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            $value = sanitize_url( $value );
            if ( ! filter_var( $value, FILTER_VALIDATE_URL ) ) return false;

            return self::set_text( $field_name, $post, sanitize_url( $value ) );
        }

        /**
         * Retrieves the value of a Button Group field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_buttongroup( string $field_name, int|string|\WP_Post $post ) : string {
            return self::get_radio( $field_name, $post );
        }

        /**
         * Set the value of a Button Group field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post   $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_buttongroup( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            return self::set_radio( $field_name, $post, $value );
        }

        /**
         * Retrieves the value of a Checkbox field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_checkbox( string $field_name, int|string|\WP_Post $post ) : array {
            return self::get_repeater( $field_name, $post );
        }

        /**
         * Set the value of a Checkbox field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post  $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_checkbox( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            return self::set_repeater( $field_name, $post, $value );
        }

        /**
         * Retrieves the value of a Radio field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_radio( string $field_name, int|string|\WP_Post $post ): string {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                $value = get_field( $field_name, $post_id );
                return is_null( $value ) ? '' : $value;
            }   

            return '';
        }

        /**
         * Set the value of a Radio field for a given post.
         * 
         * @param string              $field_name The name of the field
         * @param int|string|\WP_Post $post The post ID or WP_Post object
         * @param string              $value The field value
         * 
         * @return bool              True on successful update, false on failure.
         */
        public static function set_radio( string $field_name, int|string|\WP_Post $post, string $value ): bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                return update_field( $field_name, $value, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a Select field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return array|string         The field value or an empty string/array on error.
         */
        public static function get_select( string $field_name, int|string|\WP_Post $post ): array|string {
            $post_id = Util::get_post_id( $post );
            if( false === $post_id ) return '';

            $field_object = get_field_object( $field_name, $post );
            
            if( is_array( $field_object ) ) {

                $default_value = '';
                if( isset( $field_object[ 'default_value' ] ) ) {
                    $default_value = $field_object[ 'default_value' ];
                }

                return empty( $value ) ? $default_value : $value;
            }

            return empty( $value ) ? '' : $value;
        }

        /**
         * Set the value of a Select field for a given post.
         * 
         * @param string              $field_name The name of the field
         * @param int|string|\WP_Post $post The post ID or WP_Post object
         * @param array|string        $value The field value
         * 
         * @return bool              True on successful update, false on failure.
         */
        public static function set_select( string $field_name, int|string|\WP_Post $post, array|string $value ): bool {
            $post_id = Util::get_post_id( $post );
            if( false === $post_id ) return false;

            $field_object = get_field_object( $field_name, $post );

            if( is_array( $field_object ) ) {
                $multiple = $field_object['multiple'];

                if( $multiple ) {
                    if( is_string( $value ) ) $value = array( $value );
                } else {
                    if( is_array( $value ) ) return false;
                }
            }

            return update_field( $field_name, $value, $post_id );
        }

        /**
         * Retrieves the value of a True/False field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_truefalse( string $field_name, int|string|\WP_Post $post ) : bool {
            return boolval( self::get_text( $field_name, $post ) );
        }

        /**
         * Set the value of a True/False field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post  $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_truefalse( string $field_name, int|string|\WP_Post $post, bool $value ) : bool {
            if( ! is_bool( $value ) ) return false;
            return self::set_text( $field_name, $post, boolval( $value ) );
        }

        /**
         * Retrieves the value of a File field for a given post.
         * 
         * @param  string                $field_name    The name of the field
         * @param  int|string|\WP_Post   $post          The post ID or WP_Post object
         * @param  string                $return_format The return format. Can be passed string 'ids', 'url' or 'array'. Default 'ids'.
         * 
         * @return string|array          The field value or an empty string on error.
         */
        public static function get_file( string $field_name, int|string|\WP_Post $post, string $return_format = 'ids' ) : string|int|array {
            $post_id = Util::get_post_id( $post );
            if( false === $post_id ) return '';

            $field_value = get_field( $field_name, $post_id );
            $field_value = Util::convert_media( $field_value, $return_format );

            if( false === $field_value ) {
                return self::value_on_error( $return_format );
            }

            return $field_value;
        }

        /**
         * Set the value of a File field for a given post.
         * 
         * @param  string                $field_name    The name of the field
         * @param  int|string|\WP_Post   $post          The post ID or WP_Post object
         * @param  string|int|array      $value         The new value
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_file( string $field_name, int|string|\WP_Post $post, string|int|array $value ) : bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                return update_field( $field_name, $value, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a Oembed field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_oembed( string $field_name, int|string|\WP_Post $post ) : bool {
            return self::get_textarea( $field_name, $post );
        }

        /**
         * Set the value of a Oembed field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post  $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_oembed( string $field_name, int|string|\WP_Post $post, bool $value ) : bool {
            return self::set_textarea( $field_name, $post, $value );
        }

        /**
         * Retrieves the value of a wysiwyg field for a given post.
         * 
         * @param string    $field_name The name of the field
         * @param mixed     $post       The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_wysiwyg( string $field_name, int|string|\WP_Post $post ) : string {
            return self::get_text( $field_name, $post );
        }

        /**
         * Set the value of a wysiwyg field for a given post.
         * 
         * @param   string                  $field_name     The name of the field
         * @param   int|string|\WP_Post     $post           The post ID or WP_Post object
         * @param   string                  $value          The new value
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_wysiwyg( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                return update_field( $field_name, $value, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a Color Picker field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_colorpicker( string $field_name, int|string|\WP_Post $post ) : bool {
            return self::get_text( $field_name, $post );
        }

        /**
         * Set the value of a Color Picker field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post  $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_colorpicker( string $field_name, int|string|\WP_Post $post, bool $value ) : bool {
            $regex = '/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/';
            if( ! preg_match($regex, $color) ) return false;
            return self::set_text( $field_name, $post, $value );
        }

        /**
         * Retrieves the value of a Date Picker field for a given post.
         * 
         * @param   string                $field_name       The name of the field
         * @param   int|string|\WP_Post   $post             The post ID or WP_Post object
         * @param   string                $return_format    The datetime return format. Default : d-m-Y
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_datepicker( string $field_name, int|string|\WP_Post $post, string $return_format = 'd-m-Y' ) : string {
            $value = self::get_text( $field_name, $post );
            if( empty( $value ) ) return $value;
            
            return Util::convert_datetime( $value, $return_format );
        }

        /**
         * Set the value of a Date Picker field for a given post.
         * 
         * @param   string                $field_name   The name of the field
         * @param   int|string|\WP_Post   $post         The post ID or WP_Post object
         * @param   string                $value        The datetime string format
         * 
         * @return bool              True on successful update, false on failure.
         */
        public static function set_datepicker( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                $value = Util::convert_datetime( $value, 'Ymd' );
                $regex = '/^([0-9]{4})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])$/';

                if ( ! preg_match( $regex, $date ) ) return false;

                $field_value = get_field( $field_name, $post_id );
                if( $field_value === $value ) return true;
                
                return update_field( $field_name, $value, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a Time Picker field for a given post.
         * 
         * @param   string                $field_name       The name of the field
         * @param   int|string|\WP_Post   $post             The post ID or WP_Post object
         * @param   string                $return_format    The datetime return format. Default : H:i:s
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_timepicker( string $field_name, int|string|\WP_Post $post, string $return_format = 'H:i:s' ) : string {
            return self::get_datepicker( $field_name, $return_format );
        }

        /**
         * Set the value of a Time Picker field for a given post.
         * 
         * @param   string                $field_name   The name of the field
         * @param   int|string|\WP_Post   $post         The post ID or WP_Post object
         * @param   string                $value        The datetime string format
         * 
         * @return bool              True on successful update, false on failure.
         */
        public static function set_timepicker( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                $value = Util::convert_datetime( $value, 'H:i:s' );
                $regex = '/^([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/';

                if ( ! preg_match( $regex, $date ) ) return false;

                $field_value = get_field( $field_name, $post_id );
                if( $field_value === $value ) return true;
                
                return update_field( $field_name, $value, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a DateTime Picker field for a given post.
         * 
         * @param   string                $field_name       The name of the field
         * @param   int|string|\WP_Post   $post             The post ID or WP_Post object
         * @param   string                $return_format    The datetime return format. Default : Y-m-d H:i:s
         * 
         * @return  string                The field value or an empty string on error.
         */
        public static function get_datetimepicker( string $field_name, int|string|\WP_Post $post, string $return_format = 'Y-m-d H:i:s' ) : string {
            return self::get_datepicker( $field_name, $return_format );
        }

        /**
         * Set the value of a DateTime Picker field for a given post.
         * 
         * @param   string                $field_name   The name of the field
         * @param   int|string|\WP_Post   $post         The post ID or WP_Post object
         * @param   string                $value        The datetime string format
         * 
         * @return bool                   True on successful update, false on failure.
         */
        public static function set_datetimepicker( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                $value = Util::convert_datetime( $value, 'Y-m-d H:i:s' );
                $regex = '/^(\d{4})-(0[1-9]|1[0-2])-([0-2][0-9]|3[01]) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/';

                if ( ! preg_match( $regex, $date ) ) return false;

                $field_value = get_field( $field_name, $post_id );
                if( $field_value === $value ) return true;
                
                return update_field( $field_name, $value, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a Google Maps field for a given post.
         * 
         * @param string $field_name The name of the field
         * @param mixed $post The post ID or WP_Post object
         * 
         * @return array An array of the Google Maps field value.
         *               Data structure :
         * 
         *               array {
         *                  @type string $address             The complete address
         *                  @type float  $lat                 The latitude
         *                  @type float  $lng                 The longitude
         *                  @type int    $zoom                The zoom applied to the map
         *                  @type string $place_id            The place id
         *                  @type string $name                The name of the address
         *                  @type string $street_number       The street number
         *                  @type string $street_name         The street name
         *                  @type string $street_name_short   The street name but shortened
         *                  @type string $city                The city name
         *                  @type string $state               The state name ( region )
         *                  @type string $post_code           The postal code
         *                  @type string $country             The country name
         *                  @type string $country_short       The country letters ( ex: FR )
         *               }
         *               
         *               OR an empty array if the post ID is not valid or the field is not set.
         */
        public static function get_googlemap( string $field_name, int|string|\WP_Post $post ): array {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {

                $default_values = array (
                    'address'           => '',
                    'lat'               => '',
                    'lng'               => '',
                    'zoom'              => '',
                    'place_id'          => '',
                    'name'              => '',
                    'street_number'     => '',
                    'street_name'       => '',
                    'street_name_short' => '',
                    'city'              => '',
                    'state'             => '',
                    'post_code'         => '',
                    'country'           => '',
                    'country_short'     => '',
                );

                $value = get_field( $field_name, $post_id );
                return is_null( $value ) ? $default_values : ( $value + $default_values );
            }   

            return $default_values;
        }

        /**
         * Set the value of a Google Maps field for a given post.
         * 
         * @param   string  $field_name The name of the field
         * @param   mixed   $post       The post ID or WP_Post object
         * @param   array   An array of the Google Maps field value.
         *               Data structure :
         * 
         *               array {
         *                  @type string $address             The complete address
         *                  @type float  $lat                 The latitude
         *                  @type float  $lng                 The longitude
         *                  @type int    $zoom                The zoom applied to the map
         *                  @type string $place_id            The place id
         *                  @type string $name                The name of the address
         *                  @type string $street_number       The street number
         *                  @type string $street_name         The street name
         *                  @type string $street_name_short   The street name but shortened
         *                  @type string $city                The city name
         *                  @type string $state               The state name ( region )
         *                  @type string $post_code           The postal code
         *                  @type string $country             The country name
         *                  @type string $country_short       The country letters ( ex: FR )
         *               }
         */
        public static function set_googlemap( string $field_name, int|string|\WP_Post $post, array $value ): bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                $map = self::get_map( $field_name, $post );
                foreach( $value as $key => $v ) {
                    $map[$key] = $v;
                }
                return update_field( $field_name, $map, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a Message field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_message( string $field_name, int|string|\WP_Post $post ) : bool {
            return self::get_textarea( $field_name, $post );
        }

        /**
         * Set the value of a Oembed field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post  $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_message( string $field_name, int|string|\WP_Post $post, bool $value ) : bool {
            return self::set_textarea( $field_name, $post, $value );
        }

        /**
         * Retrieves the value of a Repeater field for a given post.
         * 
         * @param string $field_name The name of the field
         * @param mixed  $post The post ID or WP_Post object
         * 
         * @return array The array value or an empty array on error.
         */
        public static function get_repeater( string $field_name, int|string|\WP_Post $post ): array {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                $value = get_field( $field_name, $post_id );
                return is_null( $value ) ? array() : $value;
            }   

            return array();
        }

        /**
         * Set the value of a Repeater field for a given post.
         * 
         * @param   string  $field_name     The name of the field
         * @param   mixed   $post           The post ID or WP_Post object
         * @param   array   $value          The repeater values
         * 
         * @return bool      True on successful update, false on failure.
         */
        public static function set_repeater( string $field_name, int|string|\WP_Post $post, array $value ): bool {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                return update_field( $field_name, $value, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a Link field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_link( string $field_name, int|string|\WP_Post $post ) : string {
            return self::get_text( $field_name, $post );
        }

        /**
         * Set the value of a Link field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post   $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_link( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            return self::set_text( $field_name, $post, sanitize_url( $value ) );
        }

        /**
         * Retrieves the value of a PageLink field for a given post.
         * 
         * @param string                $field_name    The name of the field
         * @param int|string|\WP_Post   $post          The post ID or WP_Post object
         * 
         * @return string The field value or an empty string on error.
         */
        public static function get_pagelink( string $field_name, int|string|\WP_Post $post ) : string {
            return self::get_link( $field_name, $post );
        }

        /**
         * Set the value of a PageLink field for a given post.
         * 
         * @param   string               $field_name     The name of the field
         * @param   int|string|\WP_Post   $post          The post ID or WP_Post object
         * @param   string               $value          The new Link
         * 
         * @return  bool                 True on successful update, false on failure.
         */
        public static function set_pagelink( string $field_name, int|string|\WP_Post $post, string $value ) : bool {
            return self::set_link( $field_name, $post, sanitize_url( $value ) );
        }

        /**
         * Retrieves the value of a specified relationship field for a given post.
         *
         * @since 1.0.0
         *
         * @param string $field_name The name of the relationship field to retrieve.
         * @param int|string|\WP_Post $post The post to retrieve the relationship field from. Can be passed as a post ID or a WP_Post object.
         * @param string $return_format The return format. Can be passed string 'ids' or 'object'
         * 
         * @return array An array of the relationship field value.
         *               An empty array if the post ID is not valid or the field is not set.
         */
        public static function get_relationship( string $field_name, int|string|\WP_Post $post, string $return_format = 'ids'  ) : array {
            $post_id = Util::get_post_id( $post );

            // Return an empty array if post is not valid
            if( false === $post_id ) return array();

            $value = get_field( $field_name, $post_id, false );

            // Return an empty array in case of null value or empty array
            if( is_null( $value ) ) return array();
            if( is_array( $value ) && 0 === count( $value ) ) return $value;

            /**
             * Convert values depending on desired return format
             */
            switch( $return_format ) {
                case 'object':
                    // Check if the value is a post object
                    if( Util::is_post( $value[0] ) ) {
                        // Return the value as is
                        return $value;
                    }

                    // Check if the value is a post ID
                    if( ctype_digit( $value[0] ) ) {
                        // Convert the post IDs to post objects
                        array_walk( $value, function( &$post_id ) {
                            $post_id = get_post( $post_id );
                        } );
                        return $value;
                    }
                    break;

                default:
                    // Check if the value is a post ID
                    if( ctype_digit( $value[0] ) ) return $value;

                    // Check if the value is a post object
                    if( Util::is_post( $value[0] ) ) {
                        // Return the post IDs from the post objects
                        return wp_list_pluck( $value, 'ID' );
                    }
                    break;
            }

            return array();
        }

        /**
         * Retrieves the value of a specified relationship field for a given post.
         *
         * @since 1.0.0
         *
         * @param string                $field_name     The name of the relationship field to retrieve.
         * @param int|string|\WP_Post    $post           The post to retrieve the relationship field from. Can be passed as a post ID or a WP_Post object.
         * @param array                 $value          The array of values to update can be IDS or WP_Post
         * 
         * @return bool                 True on successful update, false on failure.
         */
        public static function set_relationship( string $field_name, int|string|\WP_Post $post, array $value  ) : bool {
            $post_id = Util::get_post_id( $post );

            // Return false if post is not valid
            if( false === $post_id ) return false;

            // Checks the array format
            if( ! is_array( $value ) ) return false;
            if( ! array_is_list( $value ) ) return false;

            // Checks min and max
            $field_object = get_field_object( $field_name, $value );

            if( is_array( $field_object ) ) {

                if( isset( $field_object['min'] ) && is_number( $field_object['min'] ) ) {
                    $min = floatval( $field_object['min'] );
                    if( count( $value ) < $min ) {
                        return false;
                    }
                }
                if( isset( $field_object['max'] ) && is_number( $field_object['max'] ) ) {
                    $max = floatval( $field_object['max'] );
                    if( count( $value ) > $max ) {
                        return false;
                    }
                }
            }

            foreach( $value as $index => $post ) {
                if( ! Util::is_int( $post ) ) {
                    if ( Util::is_post( $post ) ) {
                        $value[$index] = strval( Util::get_post_id( $post ) );
                    } else {
                        return false;
                    }
                } else {
                    // Need a string and not an int. Just in case.
                    $value[$index] = strval( $post );
                }
            }


            // Delete meta.
            $acf_valid_post_id = acf_get_valid_post_id( $post_id );
            // Delete meta.
            $delete = acf_delete_metadata( $post_id, $field_name );

            // Delete reference.
            acf_delete_metadata( $post_id, $field_name, true );

            // Delete stored data.
            acf_flush_value_cache( $post_id, $field_name );

            // Update the field
            return update_field( $field_name, $value, $post_id );
        }

        /**
         * Retrieves the value of a User field for a given post.
         * 
         * @param string                $field_name     The name of the field
         * @param int|string|\WP_Post    $post $post     The post ID or WP_Post object
         * @param int|bool|array|WP_User $return_format  The return format. Can be passed string 'ids' or 'array' or 'object' ( default )
         * 
         * @return  int|bool|array|WP_User  The ID, an Array or a WP_User
         *                                  false if not owner found
         */
        public static function get_user( string $field_name, int|string|\WP_Post $post, string $return_format = 'object' ): int|bool|array|\WP_User {
            $post_id = Util::get_post_id( $post );

            if( false !== $post_id ) {
                $value = get_field( $field_name, $post_id );
                
                if( empty( $value ) ) {
                    return false;
                }
                
                return Util::convert_user( $value, $return_format );
            }   

            return false;
        }

        /**
         * Set the value of a User field for a given post.
         * 
         * @param   string                  $field_name     The name of the field
         * @param   int|string|\WP_Post     $post $post     The post ID or WP_Post object
         * @param   int|array|WP_User       $user           The user
         * 
         * @return  bool                    True on successful update, false on failure.
         */
        public static function set_user( string $field_name, int|string|\WP_Post $post, int|array|\WP_User $user ): bool {
            $post_id = Util::get_post_id( $post );
            $user_id = Util::convert_user( $user, 'ids' );

            if( false !== $post_id ) {
                return update_field( $field_name, $user_id, $post_id );
            }   

            return false;
        }

        /**
         * Retrieves the value of a User field for a given post.
         * 
         * @param string                    $field_name     The name of the field
         * @param int|string|\WP_Post        $post $post     The post ID or WP_Post object
         * @param int|bool|array|WP_User    $return_format  The return format. Can be passed string 'ids' or 'array' or 'object' ( default )
         * 
         * @return array|bool                    On success an array of int|array|WP_User, false otherwise.
         */
        public static function get_users( string $field_name, int|string|\WP_Post $post, string $return_format = 'object' ): array|bool {
            $post_id = Util::get_post_id( $post );

            $users = array();

            if( false !== $post_id ) {
                $users = get_field( $field_name, $post_id );
                
                if( empty( $users ) || ! is_array( $users ) ) {
                    return false;
                }

                foreach( $users as &$user ) {
                    $user = Util::convert_user( $user, $return_format );
                }

                return $users;
            }   

            return false;
        }

        /**
         * Return an empty value with the desired format on error
         * 
         * @param  string                $return_format The return format. Can be passed string 'ids', 'string', 'object', 'array', 'bool.
         * 
         * @return string|array          The field value or an empty string on error.
         */
        public static function value_on_error( string $return_format ) : int|string|object|array|bool {
            switch( $return_format ) {
                case 'ids':
                    return 0;
                case 'string':
                    return '';
                case 'object':
                    return new stdClass();
                case 'array':
                    return array();
                case 'bool':
                    return false;
            }
            return false;
        }
    }
}