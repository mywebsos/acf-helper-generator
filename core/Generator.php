<?php

namespace ACF_Helper_Generator;

if( ! class_exists( 'ACF_Helper_Generator\Generator' ) ) {
    
    define('CONFIG_FILE_PATH', dirname(__DIR__) . '/config.json');

    /**
     * Utility class providing helper methods for various tasks.
     *
     * @package Acf Helper generator
     * @subpackage ACF_Helper_Generator
     * @since 1.0.0
     */
    class Generator {

        /**
         * Represents the file path of the `config.json` file located in the parent directory.
         * 
         * @var string $config_file
         */
        public static string $config_file = CONFIG_FILE_PATH;

        /**
         * The configuration data read from the config file.
         *
         * @var array
         */
        public static array  $config;
        
        /**
         * Includes all the PHP files in the specified directory and its subdirectories.
         *
         * @param string $dir The directory to search for PHP files in
         *
         * @return array An array of PHP files found in the directory and its subdirectories
         *
         * @throws InvalidArgumentException If the specified directory does not exist or is not readable
         */
        public static function get_files(string $dir): array {
            if ( ! is_dir( $dir ) || !is_readable( $dir ) ) {
                throw new \InvalidArgumentException('Directory not found');
            }

            $files = glob("$dir/{,*/,*/*/,*/*/*/,*/*/*/*/}*.php", GLOB_BRACE);
            $file_paths = [];

            foreach ($files as $file) {
                $path_info = pathinfo( $file );
                $file_paths[] = rtrim( $dir ) . '/' . $path_info['basename'];
            }

            return $file_paths;
        }

        /**
         * Reads the ACF config file and returns its content.
         *
         * @throws Exception If there is an error reading or decoding the config file.
         *
         * @return array The content of the ACF config file.
         */
        public static function read_acf_config_file() {
            $config_json = file_get_contents( self::$config_file );
            if ($config_json === false) {
                throw new \Exception('Error: Failed to read config file.');
            }

            $config_data = json_decode( $config_json, true );

            if (is_null($config_data)) {
                throw new \Exception('Error: Failed to decode config file.');
            }

            $config = [];

            foreach ($config_data['acf_config'] as $config_item) {
                $config[$config_item['name']] = $config_item['path'];
            }

            self::$config = $config;
        }


        /**
         * Get all ACF PHP files in the specified directory and generate a helper class for each one.
         *
         * @throws InvalidArgumentException if the directory is not found or not readable
         * @throws RuntimeException if a file is not found
         * 
         * @return void
         */
        public static function generate_acf_helpers( ): void {

            self::read_acf_config_file();

            $dir = getcwd() . '/' . self::$config['source'];

            if ( ! is_dir( $dir )  || ! is_readable( $dir ) ) {
                throw new \InvalidArgumentException('Directory not found');
            }

            $acf_files = self::get_files( $dir );

            foreach ( $acf_files as $acf_file ) {
                echo 'Processing: ' . str_ireplace( $dir .'/', '', $acf_file ) . ' .. ';
                if ( ! file_exists( $acf_file ) ) {
                    throw new \RuntimeException( 'File not found' );
                }

                try {
                    self::generate_hlpr( $acf_file );
                    echo "OK\n";
                } catch ( Exception $e ) {
                    // Log the error or handle it in some other way
                    error_log('Error generating helper: ' . $e->getMessage());
                }
            }
        }

        /**
         * Get all ACF PHP files in the specified directory and generate a helper class for each one.
         *
         * @throws RuntimeException if a file is not found or Text Domain is not found.
         * 
         * @return void
         */
        public static function get_namespace() : string|false {

            self::read_acf_config_file();
            $style_path = self::$config['style'];

            if ( ! file_exists( $style_path ) ) {
                throw new \RuntimeException( 'File style.css not found at the root of theme' );
            }

            // Define the pattern to match the Text Domain
            $pattern = '/Text Domain:\s*([\w-]+)/';

            // Read the contents of the style.css file
            $contents = file_get_contents( $style_path );

            // Use preg_match to extract the Text Domain value
            if ( preg_match( $pattern, $contents, $matches ) ) {
                // Print out the Text Domain value
                return str_replace( ' ', '_', self::pretty( $matches[1] ) ) ;
            }

            if ( ! file_exists( $style_path ) ) {
                throw new \RuntimeException( 'Text Domain not found in style.css, please add it.' );
            }

        }

        /**
         * Extracts the comments and declarations of public static functions from a given PHP file content.
         *
         * @param string $file_content The content of the PHP file to extract the functions from.
         * @return array An array of associative arrays, each representing a function and containing the following keys:
         *               - "comment": the comment block above the function, if any.
         *               - "function_declaration": the declaration of the function (including the "public static" keywords).
         *               - "function_code": the code inside the function.
         */
        public static function get_functions(string $file_content)
        {
            $pattern = '/\/\*\*(.*?)\*\/\s+(public static function.*?)\{(.*?)\}/s';
            preg_match_all($pattern, $file_content, $matches, PREG_SET_ORDER);

            $functions = array();
            foreach ($matches as $match) {
                $comment = trim($match[1]);
                $function_declaration = trim($match[2]) . ' {';
                $function_code = trim($match[3]);
                $functions[] = array(
                    'comment' => $comment, 
                    'function_declaration' => $function_declaration, 
                    'function_code' => "    /**\n     " . $comment . " */\n    " . $function_declaration . "  \n        " .  $function_code ."  \n    }\n\n");
            }

            return $functions;
        }

        /**
         * Get all ACF field lines in the given ACF content.
         *
         * @param  string $acf_content The ACF content to search in
         *
         * @return array  An array of all ACF field lines
         */
        public static function get_fields( string $acf_content ) : array {
            $fields = [];
            if ( empty( $acf_content ) ) {
                return $fields;
            }

            try {
                $pattern = '/\bfields\b.*((?!Group::)[A-Za-z]+)::make.*$/m';
                preg_match_all( $pattern, $acf_content, $matches );
                $fields = $matches[0];
            } catch (Exception $e) {
                // Handle the exception
                error_log('Error in get_fields function: ' . $e->getMessage());
            }
            
            return $fields;
        }

        /**
         * Get all ACF field types in the given ACF content.
         *
         * @param  string $acf_content The ACF content to search in
         *
         * @return array  An array of all ACF field types
         */
        public static function get_all_field_types(string $acf_content): array {
            $matches = [];
            if ( empty( $acf_content ) ) {
                return $matches;
            }

            try {
                $pattern = '/(\bfields|\bgroups).*\b([A-Za-z]+)::make.*$/m';
                preg_match_all( $pattern, $acf_content, $matches );
            } catch (Exception $e) {
                // Handle the exception
                error_log( 'Error in get_all_field_types function: ' . $e->getMessage() );
                return $matches;
            }

            if ( count($matches) < 2 ) {
                return $matches;
            }

            return $matches[2];
        }

        /**
         * Get all ACF group lines in the given ACF content.
         *
         * @param  string $acf_content The ACF content to search in
         *
         * @return array  An array of all ACF group lines
         */
        public static function get_groups( string $acf_content ) : array {
            $groups = array();
            if ( ! empty( $acf_content ) ) {
                try {
                    $pattern = '/\bgroups\b.*Group::make.*$/m';
                    preg_match_all( $pattern, $acf_content, $matches );
                    $groups = $matches[0];
                } catch ( Exception $e ) {
                    // Handle the exception
                    error_log( 'Error in get_groups function: ' . $e->getMessage() );
                }
            }
            return $groups;
        }

        /**
         * Get the name of an ACF field from its line.
         *
         * @param  string $field_line The ACF field line
         *
         * @return string The name of the ACF field
         * @throws InvalidArgumentException if $field_line is empty or not a string
         * @throws RuntimeException if the pattern match fails
         */
        public static function get_field_name(string $field_line): string {
            if (empty($field_line)) {
                throw new \InvalidArgumentException('$field_line is empty');
            }
            if (!is_string($field_line)) {
                throw new \InvalidArgumentException('$field_line is not a string');
            }

            $pattern = "/(?<=')[^']*(?='[^']*$)/";
            preg_match($pattern, $field_line, $matches);

            if (count($matches) < 1) {
                throw new \RuntimeException('Pattern match failed');
            }

            return $matches[0];
        }

        /**
         * Get the type of an ACF field from its line.
         *
         * @param  string $field_line The ACF field line
         *
         * @return string|false The type of the ACF field, or false if not found
         */
        public static function get_field_type( string $field_line ) {
            $pattern = "/(\w+)::make/";
            if (preg_match($pattern, $field_line, $matches)) {
                return $matches[1];
            }
            return false;
        }

        /**
         * Get the indexes of all ACF groups in the given ACF content.
         *
         * @param  string $acf_content The ACF content to search in
         *
         * @return array An array of all ACF group indexes
         */
        public static function get_group_indexs( string $acf_content ) : array {
            $group_indexs = [];

            try {
                $fields = self::get_all_field_types( $acf_content );

                for( $i = 0; $i < count( $fields ); $i++ ) {
                    if( 'Group' === $fields[$i] ) {
                        $group_indexs[] = $i;
                    }
                }
            } catch (Exception $e) {
                // Handle the exception
                error_log('Error in get_group_indexs function: ' . $e->getMessage());
            }

            return $group_indexs;
        }


        /**
         * Get the location specified in the given ACF content.
         *
         * @param string $acf_content The ACF content to search in
         *
         * @return string|null The name of the location or null if not found
         */
        public static function get_location(string $acf_content): ?string {
            $pattern = '/\b.*Location::where.*$/m';
            try {
                preg_match($pattern, $acf_content, $matches);
                return isset($matches[0]) ? self::get_field_name($matches[0]) : null;
            } catch (Exception $e) {
                // Handle the exception
                error_log('Error in get_location function: ' . $e->getMessage());
                return null;
            }
        }

        /**
         * Formats a string to be more readable by capitalizing the first
         * letter of each word and replacing hyphens and underscores with spaces.
         *
         * @param string $to_pretty The string to be formatted
         *
         * @return string The formatted string
         */
        public static function pretty( $to_pretty ) {
            return ucwords( str_replace( array( '-', '_' ), ' ', $to_pretty ) );
        }

        /**
         * Generate a helper class for an ACF group in a PHP file.
         *
         * @param string $file The file path to the ACF group.
         *
         * @return void
         */
        public static function generate_hlpr( $file ) {

            if (!file_exists($file) || !is_readable($file)) {
                throw new \RuntimeException('File not found or not readable');
            }

            // Get the ACF content from the file.
            $acf_content  = file_get_contents( $file );

            // Get the field and group lines from the ACF content.
            $fields_lines = self::get_all_field_types( $acf_content );
            $group_indexs = self::get_group_indexs( $acf_content );
            $group_index_nb = count( $group_indexs );
            $group_lines  = self::get_groups( $acf_content );
            $field_lines  = self::get_fields( $acf_content );
            $field_infos  = [];

    
            for( $i = 0; $i < count( $field_lines ); $i++ ) {
                $field_line = $field_lines[$i];
                $field_name = self::get_field_name( $field_line );
                $field_type = self::get_field_type( $field_line );
                $field_infos[] = ['group' => self::get_field_name( $group_lines[0] ), 'name' => $field_name, 'type' => strtolower( $field_type) ];
                if( ( $i + 1 ) === intval( $group_indexs[0] ) ) {
                    unset( $group_indexs[0] );
                    unset( $group_lines[0] );
                    $group_indexs = array_values( $group_indexs );
                    $group_lines = array_values( $group_lines );
                }
            }

            // Get the location from the ACF content.
            $location = self::get_location( $acf_content );
            $post_type = $location;
            $post_type_pretty = self::pretty( $post_type );

            // Generate the helper class name.
            $class_name = 'HLPR_' . str_replace( ' ', '_', ucwords( $post_type_pretty ) );

            // Generate the PHP code for the helper class.
            $namespace = self::get_namespace();
            $class_code = "<?php\n\n";
            $class_code .= "namespace $namespace;\n\n";
            $class_code .= "class $class_name { \n\n";

            // Add static variables for each unique group name.
            $group_names = [];
            foreach( $field_infos as $field_info ) {
                $group_names[] = $field_info['group'];
            }
            $group_names = array_unique( $group_names );
            
            foreach( $group_names as $group_name ) {
                $class_code .= "    public static \$group_$group_name = '" . $group_name . "';\n";
            }

            $file_path = getcwd() . '/' . self::$config["dest"] . "/$class_name.php";
            
            // Get the HELPER content from the file.
            $helper_functions = array();
            $new_helper_functions = array();
            
            if ( file_exists( $file_path ) ) {
                $helper_content  = file_get_contents( $file_path );
                $helper_functions = self::get_functions( $helper_content );
            }

            // Add get and set methods for each field in the ACF group.
            for ( $i = 0; $i < count( $field_infos ); $i++ ) {
                $field_info = $field_infos[$i];
                $group_name = $field_info['group'];
                $field_pretty = self::pretty( $field_info['name'] );

                $field_name     = $field_info['name'];
                $field_type     = $field_info['type'];
                $field_format   = strval( self::get_field_format( $field_type ) );
                $function_name_get  = "public static function get_$field_name( int|string|\WP_Post \$post_id ) : $field_format {";
                $function_name_set  = "public static function set_$field_name( int|string|\WP_Post \$post_id, $field_format \$value ) : bool {";
                $new_helper_functions[] = $function_name_get;
                $new_helper_functions[] = $function_name_set;

                $class_code .= "
    /**
     * Get the value of the $field_pretty field for an $post_type_pretty
     * 
     * @param   int|string|\WP_Post  $post_type The $post_type_pretty post or post ID
     * @return  $field_format        The value of the $field_pretty field
     * */ \n";

                $class_code .= "    public static function get_$field_name( int|string|\WP_Post \$post_id ) : $field_format { \n";
                $class_code .= "        \$field_name = \ACF_Helper_Generator\Util::concat_strings( '_', self::\$group_$group_name, '$field_name'); \n";
                $class_code .= "        return \ACF_Helper_Generator\Accessor::get_$field_type( \$field_name, '$field_name', \$post_id ); \n";
                $class_code .= "    } \n";

                $class_code .= "
    /**
     * Set the value of the $field_pretty field for an $post_type_pretty
     * 
     * @param   int|string|\WP_Post  $post_type  The $post_type_pretty post or post ID
     * @param   $field_format        \$value     The new $field_pretty
     * 
     * @return  bool                 True on successful update, false on failure.
     * */ \n";
                $class_code .= "    public static function set_$field_name( int|string|\WP_Post \$post_id, $field_format \$value ) : bool { \n";
                $class_code .= "        \$field_name = \ACF_Helper_Generator\Util::concat_strings( '_', self::\$group_$group_name, '$field_name'); \n";
                $class_code .= "        return \ACF_Helper_Generator\Accessor::set_$field_type( \$field_name, '$field_name', \$post_id, \$value ); \n";
                $class_code .= "    } \n";
            }

            $class_code .= "\n";

            foreach( $helper_functions as $helper_function ) {
                $function_declaration = $helper_function['function_declaration'];
                
                if( ! in_array( $function_declaration, $new_helper_functions ) ) {
                    $class_code .= $helper_function['function_code'];
                }
            }
            
            $class_code .= "} \n";

            if ( file_exists( $file_path ) ) {
                unlink( $file_path );
            }
            file_put_contents( $file_path, $class_code, FILE_APPEND | LOCK_EX );

        }

        /**
         * Get the format of a given ACF field type
         *
         * @param string $field_type The type of the ACF field
         *
         * @return string|null The format of the given ACF field type, or null if not found
         */
        public static function get_field_format(string $field_type): ?string {
            $acf_hlpr_path = __DIR__ . '/Accessor.php';

            if ( ! is_file( $acf_hlpr_path ) || ! is_readable( $acf_hlpr_path ) ) {
                throw new \RuntimeException( 'Accessor.php not found or is not readable' );
            }
            $acf_hlpr_content = file_get_contents($acf_hlpr_path);
            if ($acf_hlpr_content === false) {
                throw new \RuntimeException('Failed to read Accessor.php');
            }
            $matches = [];
            try {
                preg_match_all("/function\s+set_(\w+)\s*\(\s*([^\)]*)\s*\)\s*:\s*(\w+)/", $acf_hlpr_content, $matches);
            } catch (Exception $e) {
                // Handle the exception
                error_log('Error in get_field_format function: ' . $e->getMessage());
                return null;
            }

            $raw_types = $matches[2];
            foreach ($raw_types as &$raw_type) {
                $raw_type_temp = explode(', ', $raw_type);
                $raw_type_temp = explode(' ', $raw_type_temp[2]);
                $raw_type = $raw_type_temp[0];
            }
            $types = $matches[1];
            for ($i = 0; $i < count($types); $i++) {
                if ($types[$i] === $field_type) {
                    return $raw_types[$i];
                }
            }
            return null;
        }
    }
}